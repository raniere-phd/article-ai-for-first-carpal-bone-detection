---
always_allow_html: true
---

# Materials and Methods

```{r}
devtools::load_all()
library(tidyverse)
library(plotly)
```

## Deep Convolutional Neural Networks Selection

Deep convolutional neural networks architectures for object detection are based on R-CNN [@girshickFastRCNN2015] or YOLO [@redmonYouOnlyLook2016]. We selected YOLOv5 [@jocherUltralyticsYolov5V62022], an architecture based on YOLO, because YOLO is faster than R-CNN [@redmonYouOnlyLook2016; @dobrovolnySpermcellDetectionUsing2022], showed promising results in other computer vision on radiographs studies [@sonavaneDentalCavityDetection2022], and it's available under an open-source licence.
We selected YOLOv5s from the YOLOv5 architecture family for its "small" number of model parameters (7.2 million), which translates to smaller RAM requirement and a faster training and detection time.

## Data Acquisition

```{r}
n_racehorses <-
  radiographs |>
  pull(id) |>
  unique() |>
  length()
```

We collected `r n_racehorses` sets of anonymised pre-sale radiographs of racehorses from The Hong Kong Jockey Club [@dasilvaEquineRadiographClassification2022].
Each set was made by one of 10 veterinary clinics outside of Hong Kong that used their own imaging devices systems and databases.
Each set comprised 48 or more radiographs of the appendicular skeleton in DICOM format with acquisition modality attribute value of CR or DX [@nationalelectricalmanufacturersassociationNEMAPS3ISO2021].
Anonymisation removed details of acquisition hardware used by the veterinary clinics.
From each pre-sale radiograph set, we extracted the dorsal 75º medial to palmarolateral oblique (DMPLO) projection of the left and right carpi.
(@fig-radiograph).

::: {#fig-radiograph layout-ncol="2"}
![With first carpal bone.](img/0024.png)

![Absent of first carpal bone.](img/0210.png)

![With first carpal bone and text burned in cover by black rectangle.](img/0201.png)

![Absent of first carpal bone and text burned in cover by black rectangle.](img/0178.png)

Illustrative left carpus dorsal 75º medial to palmarolateral oblique radiographs.
:::

The carpus DMPLO radiograph was selected for this study because it represents a complex anatomical structure with multiple different bones superimposed or juxtaposed in a single joint and one of the bones visible on this projection, the first carpal bone, is variably present (it is a vestigal remnant present in approximately 25% of horses [@losonskyPREVALENCEDISTRIBUTIONFIRST1988; @simonRADIOLOGICANATOMICVARIATION2010]).
Furthermore, the carpus DMPLO radiograph was selected because it is used in the treatment of fracture of the accessory carpal bone where computer-assisted orthopaedic surgery was recently introduced [@depreuxAccessoryCarpalBone2022].

## Data Annotation

```{r}
n_racehorses_with_first_carpal_bone <- 
  carpus_dmplo |>
  filter(first.carpal.bone == TRUE) |>
  pull(horse_id) |>
  unique() |>
  length()

n_radiograph_with_first_carpal_bone <- 
  carpus_dmplo |>
  filter(first.carpal.bone == TRUE) |>
  nrow()
```

Carpus DMPLO radiographs in the original resolution (@fig-radiographs_resolution) were annotated with the location of radius, proximal row of carpal bones, distal row of carpal bones, accessory carpal bone, first carpal bone (if present), and metacarpus (metacarpal II, III, and IV) in the format of rectangular bounding boxes (@fig-radiograph-with-annotation) by the first author using a self hosted instance of Label Studio v1.4.1 (Heartex).
The bounding boxes were manually reviewed by the second author comparing the bounding boxes to the radiographs using the same instance of Label Studio.
After review, all bounding boxes were exported into YOLO Darknet format [@redmonYouOnlyLook2016] using Label Studio.
The first carpal bone was present in `r (100 * n_radiograph_with_first_carpal_bone / nrow(carpus_dmplo)) |> round()`% images and the average bounding box size of the first carpal bone was `r all_annotations |> filter(name == "first carpal bone") |> pull(bone_percentage_area) |> mean() |> magrittr::multiply_by(100) |> round(digits = 1)`% of the original image (@tbl-annotation-size-stats and @tbl-annotation-size-stats-yolo).

```{r}
radiographs_resolution_vis <-
  radiographs_resolution_df |>
	ggplot(aes(x = width, y = height, size=n, color=n)) +
  geom_point() +
  theme_linedraw() +
  theme(legend.position="top") +
	labs(
		x = "Width",
		y = "Height",
		size = 'Number of radiographs',
		color = 'Number of radiographs'
	)
```

::: {#fig-radiographs_resolution}
::: {.content-visible unless-format="html"}
```{r}
#| output: true
#| label: radiographs_resolution_vis

radiographs_resolution_vis
```
:::

::: {.content-visible when-format="html"}
```{r}
#| output: true

radiographs_resolution_vis |>
  ggplotly()
```
:::

Distribution of radiographs original resolution.
:::

::: {#fig-radiograph-with-annotation layout-ncol="2"}
![With first carpal bone.](img/0024-with-annotation.png){width="5cm"}

![Without first carpal bone.](img/0210-with-annotation.png){width="5cm"}

![With first carpal bone and text burned in cover by black rectangle.](img/0201-with-annotation.png){width="5cm"}

![Without first carpal bone and text burned in cover by black rectangle.](img/0178-with-annotation.png){width="5cm"}

Illustration of bounding boxes of left carpus dorsal 75º medial to palmarolateral oblique radiographs.
:::

```{r}
yolo_size <- 640 ** 2

bounding_boxes_info <- all_annotations |>
  group_by(name) |>
  summarise(
    min_pixels = min(area),
    mean_pixels = round(mean(area)),
    median_pixels = round(median(area)),
    max_pixels = max(area),
    min_yolo = round(min(bone_percentage_area) * yolo_size),
    mean_yolo = round(mean(bone_percentage_area) * yolo_size),
    median_yolo = round(median(bone_percentage_area) * yolo_size),
    max_yolo = round(max(bone_percentage_area) * yolo_size),
  ) |>
  ungroup() |>
  left_join(sorting_bones, by="name") |>
  arrange(sorting_index)
```

```{r}
#| output: true
#| label: tbl-annotation-size-stats
#| tbl-cap: Descriptive statistics of bounding box area (in pixels) in the original resolution.

 bounding_boxes_info |>
  select(
    name,
    min_pixels,
    mean_pixels,
    median_pixels,
    max_pixels
  ) |>
  knitr::kable(
    col.names = c(
      'Bone',
      'Min',
      'Mean',
      'Median',
      'Max'
    )
  )
```

```{r}
#| output: true
#| label: tbl-annotation-size-stats-yolo
#| tbl-cap: Descriptive statistics of bounding box area (in pixels) in the low resolution ($640 \times 640$-pixel) used by YOLOv5s.

 bounding_boxes_info |>
  select(
    name,
    min_yolo,
    mean_yolo,
    median_yolo,
    max_yolo
  ) |>
  knitr::kable(
    col.names = c(
      'Bone',
      'Min',
      'Mean',
      'Median',
      'Max'
    )
  )
```

## Data Split

Radiographs were split into 6 data sets using scikit-learn (version 1.0.2) that the data sets were balanced regarding the number of radiographs with the first carpal bone present or not (@fig-radiographs_split).

```{r}
radiographs_split_vis <-
  carpus_dmplo |>
	ggplot(aes(x = kfold, fill=first.carpal.bone)) +
  geom_bar() +
  scale_fill_manual(values=c("#bd0026", "#fed976")) +
  theme_linedraw() +
  theme(legend.position="top") +
	labs(
		x = "Data set ID",
		y = "Number of radiographs",
		fill = "First carpal bone is present"
	)
```

::: {#fig-radiographs_split}
::: {.content-visible unless-format="html"}
```{r}
#| output: true
#| label: radiographs_split_vis

radiographs_split_vis
```
:::

::: {.content-visible when-format="html"}
```{r}
#| output: true

radiographs_split_vis |>
  ggplotly()
```
:::

Distribution of radiographs across datasets.
:::

One of the data sets was named test (data set ID = 5) and the other 5 sets were used for leave-one-out cross-validation (identified by the data set ID of the left out data set).
Each of the 5 sets for training (left out data set ID = 0-4) was used to generate subsets with different numbers of radiographs (@tbl-secondary-dataset; full protocol details at doi:10.6084/m9.figshare.21061951).
A Python script was used to create the data for each experiment in the YOLO Darknet format [@redmonYouOnlyLook2016].

```{r}
#| output: true
#| label: tbl-secondary-dataset
#| tbl-cap: Composition of Secondary Dataset.

all_metrics |>
  filter(
    Class == 'all',
    epoch == 'test',
    fold == 0
  ) |>
  select(
    size,
    number_of_radiographs_with_first_carpal,
    number_of_radiographs_without_first_carpal,
    total_number_of_radiographs
  ) |>
  knitr::kable(
    col.names = c(
      'Secondary Dataset ID',
      'Radiographs with first carpal bone',
      'Radiographs absent of first carpal bone',
      'Total number of radiographs'
    )
  )
```

## Training

Training based on leave-one-out cross-validation was conducted for 32 epochs using YOLOv5s architecture [@jocherUltralyticsYolov5V62022] and started with a model pretrained on the COCO dataset [@linMicrosoftCOCOCommon2015] (version 6.2).
YOLOv5s used $640 \times 640$-pixel RGBs images as input and batch size of 16.
YOLOv5s uses data augmentation and combines parts of multiple images to create new ones (@fig-data-augmentation).

::: {#fig-data-augmentation layout-ncol="2"}
![](img/data-augmentation-1.png){width="5cm"}

![](img/data-augmentation-2.png){width="5cm"}

![](img/data-augmentation-3.png){width="5cm"}

![](img/data-augmentation-4.png){width="5cm"}

Illustrative input image used during training by YOLOv5s.
Input image is the result of combine parts of multiple images.
Bounding boxes are included for reference to the reader.
0: radius.
1: proximal carpal row.
2: distal carpal row.
3: acessory carpal.
4: metacarpal.
5: first carpal bone.
:::

At the end of each epoch, metrics (precision, recall, and mean average precision (mAP) [@everinghamPascalVisualObject2010; @everinghamPascalVisualObject2015]) were calculated using the validation data set (the one left out) and stored for further analysis. For calculation of the metrics, all bounding boxes calculated by the model were classified into true positive, true negative, false positive, or false negative using bounding box evaluation [@everinghamPascalVisualObject2010; @everinghamPascalVisualObject2015] that is part of YOLOv5s.

Computation was performed on a Precision 5820 workstation (Dell Hong Kong) equipped with an Intel(R) Xeon(R) W-2123 3.60GHz CPU, 64GiB (4 × 16GiB DDR4 2666MHz) of RAM, and NVIDIA Quadro RTX 4000 GPU (8 GB GDDR6) running Ubuntu 21.10 (Linux 5.13), Python (version 3.9.9), NumPy (version 1.21.2), PyTorch (version 1.10.2), and CUDA (version 11.3).

## Testing

The best model of each leave-one-out cross-validation iteration was tested on the Precision 5820 workstation (Dell Hong Kong) described previously using the test data set and metrics (precision, recall, and mAP0.5) were calculated and stored for further analysis.

## Analysis

Analysis of all metrics collected was conducted using descriptive statistics and data visualisation computed with `r version$version.string` on the Precision 5820 workstation (Dell Hong Kong) described previously.
