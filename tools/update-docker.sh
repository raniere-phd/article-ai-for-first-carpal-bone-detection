#!/bin/bash
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/article-ai-for-first-carpal-bone-detection"
SERVICE_NAME="ci"

docker build \
       --no-cache \
       --pull \
       --quiet \
       --target $SERVICE_NAME \
       --tag $DOCKER_IMAGE \
       .
docker push \
       --quiet \
       $DOCKER_IMAGE
