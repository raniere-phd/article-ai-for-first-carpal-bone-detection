# Article AI for First Carpal Bone Detection

Preview: https://raniere-phd.gitlab.io/article-ai-for-first-carpal-bone-detection

## Contributing

1.  Run `docker compose up` in the command line to launch [RStudio](https://www.rstudio.com/).\
    ![](img/login.png)
2.  Fill the login form using `rstudio` as username and `123` as password.\
    ![](img/home.png)
3.  Click in `Project` at the top left corner and `Open Project …`.\
    ![](img/open-project.png)
4.  Select `inst/int.Rproj`.\
    ![](img/quarto-file.png)
5.  When you open the `.qmd` files, you will have a button `Render`. By clicking in `Render`, the HTML version of the document will be display.\
    ![](img/quarto-render.png)

